package com.tracklocalizator.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tracklocalizator.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
