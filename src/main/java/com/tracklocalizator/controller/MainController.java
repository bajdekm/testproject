package com.tracklocalizator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tracklocalizator.model.User;
import com.tracklocalizator.service.UserService;

@Controller
public class MainController {
	
	@Autowired
	UserService userService;
	
	@RequestMapping("/")
	public String index(){
		User user = new User();
		userService.saveUser(user);
		return "index";
	}

}
