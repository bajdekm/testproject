package com.tracklocalizator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrackLocalizatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrackLocalizatorApplication.class, args);
	}
}
