package com.tracklocalizator.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tracklocalizator.model.User;
import com.tracklocalizator.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserServiceImpl implements UserService{
	
	@Autowired
	UserRepository userRepository;
	

	@Override
	public void saveUser(User user) {
		userRepository.save(user);
		log.debug("user " + user + " created");
	}

	@Override
	public User findUserByID(Long id) {
		return userRepository.findOne(id);
	}

	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public long countAllUsers() {
		long userAmount = userRepository.count();
		log.debug("users amount: " + userAmount);
		return userAmount;
	}

	@Override
	public User updateUser(User user) {
		log.debug("User " + user + " updated");
		return userRepository.save(user);
	}

	@Override
	public void deleteUser(User user) {
		userRepository.delete(user);
		log.debug("user " + user + " deleted");
		
	}

}
