package com.tracklocalizator.service;

import java.util.List;

import com.tracklocalizator.model.User;

public interface UserService {
	
	void saveUser(User user);
	User findUserByID(Long id);
	List<User> getAllUsers();
	long countAllUsers();
	User updateUser(User user);
	void deleteUser(User user);

}
